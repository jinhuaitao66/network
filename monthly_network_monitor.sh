#!/bin/bash

# 设置配置文件路径
config_file="$HOME/monthly_network_monitor_config"

# 检查是否已经安装了所需软件包，如果没有，则安装它们
if ! command -v jq &> /dev/null; then
    echo "安装 jq..."
    sudo apt update
    sudo apt install -y jq
fi

if ! command -v vnstat &> /dev/null; then
    echo "安装 vnstat..."
    sudo apt update
    sudo apt install -y vnstat
fi

if ! command -v bc &> /dev/null; then
    echo "安装 bc..."
    sudo apt update
    sudo apt install -y bc
fi

# 获取当前日期中的天
current_day=$(date +"%d")

# 获取当前月份
current_month=$(date +"%m")

# 检查配置文件是否存在，如果存在则读取日期、流量阈值和执行间隔，否则提示用户输入这些信息
if [ -f "$config_file" ]; then
    custom_day=$(jq -r '.custom_day' "$config_file")
    threshold=$(jq -r '.threshold' "$config_file")
    interval=$(jq -r '.interval' "$config_file")
else
    read -p "请输入每月的日期（1-31）： " custom_day
    read -p "请输入每月的流量阈值（GB）： " threshold
    read -p "请输入执行脚本的间隔时间（分钟）： " interval
    echo "{\"custom_day\": $custom_day, \"threshold\": $threshold, \"interval\": $interval}" > "$config_file"
fi

# 检查是否已经添加了cron作业
if ! crontab -l | grep -q '/monthly_network_monitor.sh'; then
    # 添加cron作业
    (crontab -l 2>/dev/null; echo "*/$interval * * * * /bin/bash /monthly_network_monitor.sh") | sudo crontab -
fi

# 仅在自定义日期触发时执行
if [ "$current_day" -eq "$custom_day" ]; then
    # 获取当前月份的出入流量总和
    traffic=$(vnstat --json | jq '.interfaces[0].traffic.months[] | {rx: .rx, tx: .tx}' | jq -s add)
    used_gb=$(echo "$traffic" | jq '.rx + .tx' | awk '{print $1/1024/1024/1024}')

    # 获取本月已使用流量
    echo "本月已使用的出入网络流量总和为 $used_gb GB。"

    # 比较流量与阈值
    if (( $(echo "$used_gb >= $threshold" | bc -l) )); then
        echo "本月已使用的出入网络流量总和已达到 $threshold GB，系统将自动关机。"
        shutdown -h now
    else
        echo "本月已使用的出入网络流量总和仍在安全范围内。"
    fi
fi
