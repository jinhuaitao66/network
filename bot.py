import subprocess
import re
import requests
import json
from datetime import timedelta
import sys
import locale
import schedule
import time
import os

locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
sys.stdin.reconfigure(encoding='utf-8')

CONFIG_FILE = 'config.json'

# 全局变量
send_interval = None

# 获取默认路由的网络接口
def get_default_interface():
    command = 'ip route | grep default'
    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    output = result.stdout.strip()
    interface = re.search(r'dev (\S+)', output).group(1)
    return interface

# 获取 VPS 的 IPv4 地址
def get_ipv4_address(interface):
    command = f'ifconfig {interface} | grep inet'
    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    output = result.stdout.strip()
    ipv4_address = re.search(r'inet (\S+)', output).group(1)
    return ipv4_address

# 获取 VPS 的 IPv6 地址
def get_ipv6_address(interface):
    command = f'ifconfig {interface} | grep inet6 | grep -v fe80'
    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    output = result.stdout.strip()
    ipv6_addresses = re.findall(r'inet6 (\S+)', output)
    return ipv6_addresses

# 获取 VPS 的开机时间
def get_uptime():
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = float(f.readline().split()[0])
    uptime_string = str(timedelta(seconds=uptime_seconds))
    return uptime_string

# 获取网络接口的统计信息
def get_network_stats(interface):
    command = f'ifconfig {interface}'
    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    output = result.stdout
    return output

# 发送消息到 Telegram
def send_message(token, chat_id, message):
    url = f'https://api.telegram.org/bot{token}/sendMessage'
    payload = {
        'chat_id': chat_id,
        'text': message,
        'parse_mode': 'HTML'
    }
    response = requests.post(url, json=payload)
    print(response.text)

# 获取用户输入的配置信息
def get_config():
    config = {}
    try:
        with open(CONFIG_FILE, 'r') as file:
            config = json.load(file)
    except FileNotFoundError:
        print("配置文件不存在")
    except json.JSONDecodeError:
        print("配置文件格式错误")
    return config

# 保存配置信息到文件
def save_config(config):
    with open(CONFIG_FILE, 'w') as file:
        json.dump(config, file)

# 创建 systemd 服务单元文件并启动服务
def create_systemd_service():
    service_content = """
    [Unit]
    Description=My Bot Service
    After=network.target

    [Service]
    ExecStart=/usr/bin/python3 /root/bot.py
    WorkingDirectory=/root
    StandardOutput=append:/var/log/bot.log
    StandardError=append:/var/log/bot.log
    Restart=always
    User=root

    [Install]
    WantedBy=multi-user.target
    """

    # Write the service unit file
    with open('/etc/systemd/system/my_bot.service', 'w') as f:
        f.write(service_content)

    # Reload systemd to pick up the new service unit
    subprocess.run(['systemctl', 'daemon-reload'])

    # Enable the service to start on boot
    subprocess.run(['systemctl', 'enable', 'my_bot'])

    # Start the service
    subprocess.run(['systemctl', 'start', 'my_bot'])

# 主函数
def main():
    global send_interval  # 声明全局变量

    # 获取配置信息
    config = get_config()

    # 如果配置文件中没有保存 API token 和 chat_id，则要求用户输入
    if 'token' not in config or 'chat_id' not in config:
        token = input("请输入 Telegram Bot 的 API token: ")
        chat_id = input("请输入你的 Telegram 用户 ID: ")
        vps_name = input("请输入当前 VPS 的名称: ")
        send_interval = int(input("请输入发送消息的间隔时间（秒）: "))  # 用户输入发送消息的间隔时间
        # 将配置信息保存到文件
        config['token'] = token
        config['chat_id'] = chat_id
        config['vps_name'] = vps_name
        config['send_interval'] = send_interval  # 将发送消息的间隔时间保存到配置文件中
        save_config(config)
        # 第一次配置完成后退出脚本
        sys.exit()

    else:
        token = config['token']
        chat_id = config['chat_id']
        vps_name = config['vps_name']
        send_interval = config.get('send_interval', 60)  # 默认间隔时间为60秒

    # 获取默认路由的网络接口
    interface = get_default_interface()
    # 获取 VPS 的 IPv4 地址
    ipv4_address = get_ipv4_address(interface)
    # 获取 VPS 的 IPv6 地址
    ipv6_addresses = get_ipv6_address(interface)
    # 获取 VPS 的开机时间
    uptime = get_uptime()
    # 获取网络接口的统计信息
    network_stats = get_network_stats(interface)
    
    # 提取接收和发送的数据量
    rx_match = re.search(r'RX packets (\d+)', network_stats)
    tx_match = re.search(r'TX packets (\d+)', network_stats)
    
    if rx_match and tx_match:
        rx_packets = int(rx_match.group(1))
        tx_packets = int(tx_match.group(1))
        
        # 假设一个数据包为 1500 字节，计算字节数
        rx_bytes = rx_packets * 1500
        tx_bytes = tx_packets * 1500
        
        # 将字节数转换为 GB
        rx_gb = rx_bytes / (1024 * 1024 * 1024)
        tx_gb = tx_bytes / (1024 * 1024 * 1024)
        
        # 计算接收和发送的总和
        total_gb = rx_gb + tx_gb
        
        # 构建消息
        message = f'<b>{vps_name}</b>\n\n<b>IPv4 地址:</b> {ipv4_address}\n<b>IPv6 地址:</b> {", ".join(ipv6_addresses)}\n<b>接收流量:</b> {rx_gb:.2f} GB\n<b>发送流量:</b> {tx_gb:.2f} GB\n<b>总计:</b> {total_gb:.2f} GB\n<b>开机时间:</b> {uptime}'
        
        # 发送消息到 Telegram
        send_message(token, chat_id, message)

# 创建 systemd 服务单元文件并启动服务
create_systemd_service()

# 主函数
if __name__ == "__main__":
    main()

    # 如果配置文件存在，则后台运行脚本
    if os.path.exists(CONFIG_FILE):
        # 设置定时任务，每隔 send_interval 秒调用一次 main() 函数
        schedule.every(send_interval).seconds.do(main)

        # 不断运行，直到脚本被手动终止
        while True:
            schedule.run_pending()
            time.sleep(1)
